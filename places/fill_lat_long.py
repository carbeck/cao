#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv
import geocoder
import time

# Read rows into list 'places', each record a dict
places = list()
with open('cities_de_at_ch_lu_li_fr.csv', mode='r') as filein:
    reader = csv.DictReader(filein)
    for row in reader:    
        places += [row]

# Get lat/long and write out data
with open('cities_de_at_ch_lu_li_fr-coord-temp.csv', mode='w') as fileout:

    # ATTENTION: GOOGLE IMPOSES A RATE LIMIT OF APPROX. 2500 REQUESTS PER DAY, 
    # AND 50 REQUESTS PER SECOND FOR FREE USE
    # https://developers.google.com/maps/documentation/geocoding/usage-limits
    
    rows = len(places)
    if rows > 2500:
        print('''ATTENTION: Google imposes a rate limit of 2,500 free requests per 
day. You are trying to make {} requests. Only the first 2,500 of these 
will be answered if you haven\'t already made some.'''.format(rows))

    fieldnames = sorted(places[0].keys())
    writer = csv.DictWriter(fileout, fieldnames=fieldnames)
    writer.writeheader()
    
    for i, row in enumerate(places):
        print('Getting coordinates for {} ({}/{})'.format(row['city'], i+1, rows))
    
        # Get lat and long for each place
        g = geocoder.google('{}, {}'.format(
                row['city'],
                row['country_long']
            ), components=row['country'])
        
        if g.status == 'OVER_QUERY_LIMIT':
            print('Query limit exceeded after {} elements'.format(i))
            break
        elif g.status != 'OK':
            print('Error: {}'.format(g.status))
        else:
            row['lat'] = g.lat
            row['long'] = g.lng
        
        # Write out current line with lat/long info added, if available
        writer.writerow(row)
        
        # Play halfway nice
        time.sleep(2)
