SELECT
	t.deedno_cao_long AS deed,
	t."row",
	l."text",
	t."token" AS wordform,
	t.lemma,
	t.confidence_lemma,
	t.annotation,
	t.confidence_annotation,
	places.place,
	places.x,
	places.y,
	CAST(EXTRACT(YEAR FROM mc.date_from) AS integer) AS year_from, CAST(EXTRACT(YEAR FROM mc.date_to) AS integer) AS year_to
FROM
	tokens t
INNER JOIN lines l
ON (
	l."row" = t."row"
)
INNER JOIN meta_places mp
ON (
    t.deedno_cao_long = mp.deedno_cao_long
)
INNER JOIN meta_cao mc
ON (
    mp.deedno_trier = mc.deedno_trier
)
INNER JOIN (
    SELECT deedno_trier, COUNT(id_place_trier)
    FROM meta_places
    GROUP BY deedno_trier
    HAVING COUNT(id_place_trier) = 1
) AS deedno_trier_unique
ON (
    mp.deedno_trier = deedno_trier_unique.deedno_trier
)
INNER JOIN places
ON (
    mp.id_place_trier = places.id_place_trier
)
WHERE
	lemma = 'zwêne'
	-- AND confidence_lemma >= 0.95
	-- AND confidence_annotation >= 0.95
	AND places.x != 0
	AND EXTRACT(YEAR FROM mc.date_to) - EXTRACT(YEAR FROM mc.date_from) <= 0
ORDER BY t.deedno_cao_long, l.vol, l.page, l."line"
;
