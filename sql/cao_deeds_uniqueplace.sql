﻿-- Urkunden, denen genau 1 Ort zugewiesen ist
SELECT id_place_trier, dt.deedno_trier FROM meta_places
JOIN (
	SELECT deedno_trier, COUNT(id_place_trier) AS count
	FROM meta_places
	GROUP BY deedno_trier
	HAVING COUNT(id_place_trier) = 1
) AS dt ON meta_places.deedno_trier = dt.deedno_trier