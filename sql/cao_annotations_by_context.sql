/*** ne … Ø ***/
SELECT
	lines."deedno_cao_long",
	lines."row",
	lines."text",
	"token",
	places.place,
	places.x,
	places.y,
	meta_cao.date_from,
	meta_cao.date_to
FROM lines
JOIN (
	-- Mit Hilfe von https://stackoverflow.com/a/53098980
	-- sowie https://stackoverflow.com/a/50158465/10590116
	SELECT DISTINCT "row", lemma, token, annotation
	FROM (
	    SELECT "row", "token", annotation, lemma,
	        array_agg(annotation) OVER w as surrounded_annos      -- 2
	       ,array_agg(lemma) OVER w AS surrounded_lemmas
	       ,array_agg("token") OVER w AS surrounded_tokens
	    FROM tokens
	    WINDOW w AS (                                             -- 1
	        ORDER BY "row", tokenno ASC
	        ROWS BETWEEN 5 PRECEDING AND 5 FOLLOWING
	    )
	    ORDER BY "row", tokenno ASC
	) AS "window"
	WHERE
	    lemma LIKE 'ne%'
	    AND annotation LIKE 'PTKNEG%'
	    AND NOT 'niht' = ANY("window".surrounded_lemmas)
) AS subq ON lines."row" = subq."row"
JOIN meta_places ON lines.deedno_cao_long = meta_places.deedno_cao_long
JOIN meta_cao ON meta_places.deedno_trier = meta_cao.deedno_trier
JOIN (
    SELECT deedno_trier, COUNT(id_place_trier)
    FROM meta_places
    GROUP BY deedno_trier
    HAVING COUNT(id_place_trier) = 1
) AS deedno_trier_unique ON meta_places.deedno_trier = deedno_trier_unique.deedno_trier
JOIN places ON meta_places.id_place_trier = places.id_place_trier
WHERE
    places.x != 0
    AND places.y != 0
    AND EXTRACT(YEAR FROM meta_cao.date_to) - EXTRACT(YEAR FROM meta_cao.date_from) = 0
ORDER BY length(lines."deedno_cao_long"), lines."row", lines."text" ASC
;

/*** Ø … niht ***/
SELECT
	lines."deedno_cao_long",
	lines."row",
	lines."text",
	"token",
	places.place,
	places.x,
	places.y,
	meta_cao.date_from,
	meta_cao.date_to
FROM lines
JOIN (
	-- Mit Hilfe von https://stackoverflow.com/a/53098980
	-- sowie https://stackoverflow.com/a/50158465/10590116
	SELECT DISTINCT "row", lemma, "token", annotation
	FROM (
	    SELECT "row", "token", annotation, lemma,
	        array_agg(annotation) OVER w AS surrounded_annos      -- 2
	       ,array_agg(lemma) OVER w AS surrounded_lemmas
	       ,array_agg("token") OVER w AS surrounded_tokens
	    FROM tokens
	    WINDOW w AS (                                             -- 1
	        ORDER BY "row", tokenno ASC
	        ROWS BETWEEN 5 PRECEDING AND 5 FOLLOWING
	    )
	    ORDER BY "row", tokenno ASC
	) AS "window"
	WHERE
	    lemma LIKE 'niht'
	    AND 'PTKNEG' !~ ALL("window".surrounded_annos)
) AS subq ON lines."row" = subq."row"
JOIN meta_places ON lines.deedno_cao_long = meta_places.deedno_cao_long
JOIN meta_cao ON meta_places.deedno_trier = meta_cao.deedno_trier
JOIN (
    SELECT deedno_trier, COUNT(id_place_trier)
    FROM meta_places
    GROUP BY deedno_trier
    HAVING COUNT(id_place_trier) = 1
) AS deedno_trier_unique ON meta_places.deedno_trier = deedno_trier_unique.deedno_trier
JOIN places ON meta_places.id_place_trier = places.id_place_trier
WHERE
    places.x != 0
    AND places.y != 0
    AND EXTRACT(YEAR FROM meta_cao.date_to) - EXTRACT(YEAR FROM meta_cao.date_from) = 0
ORDER BY length(lines."deedno_cao_long"), lines."row", lines."text" ASC
;

/*** ne … niht ***/
SELECT
	lines."deedno_cao_long",
	lines."row",
	lines."text",
	"token",
	places.place,
	places.x,
	places.y,
	meta_cao.date_from,
	meta_cao.date_to
FROM lines
JOIN (
	-- Mit Hilfe von https://stackoverflow.com/a/53098980
	-- sowie https://stackoverflow.com/a/50158465/10590116
	SELECT DISTINCT "row", lemma, "token", "window".surrounded_tokens, annotation
	FROM (
	    SELECT "row", "token", annotation, lemma,
	        array_agg(annotation) OVER w as surrounded_annos      -- 2
	       ,array_agg(lemma) OVER w AS surrounded_lemmas
	       ,array_agg(token) OVER w AS surrounded_tokens
	    FROM tokens
	    WINDOW w AS (                                             -- 1
	        ORDER BY "row", tokenno ASC
	        ROWS BETWEEN 5 PRECEDING AND 5 FOLLOWING
	    )
	    ORDER BY "row", tokenno ASC
	) AS "window"
	WHERE
	    lemma LIKE 'ne%'
	    AND annotation LIKE 'PTKNEG%'
	    AND 'niht' = ANY("window".surrounded_lemmas)
) AS subq ON lines."row" = subq."row"
JOIN meta_places ON lines.deedno_cao_long = meta_places.deedno_cao_long
JOIN meta_cao ON meta_places.deedno_trier = meta_cao.deedno_trier
JOIN (
    SELECT deedno_trier, COUNT(id_place_trier)
    FROM meta_places
    GROUP BY deedno_trier
    HAVING COUNT(id_place_trier) = 1
) AS deedno_trier_unique ON meta_places.deedno_trier = deedno_trier_unique.deedno_trier
JOIN places ON meta_places.id_place_trier = places.id_place_trier
WHERE
    places.x != 0
    AND places.y != 0
    AND EXTRACT(YEAR FROM meta_cao.date_to) - EXTRACT(YEAR FROM meta_cao.date_from) = 0
ORDER BY length(lines."deedno_cao_long"), lines."row", lines."text" ASC
;
