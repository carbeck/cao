﻿-- Städte nach Anzahl der ortseindeutigen Urkunden
SELECT place, x, y, ud.count
FROM places
INNER JOIN (
	-- Urkunden, denen genau 1 Ort zugewiesen ist
	SELECT id_place_trier, COUNT(id_place_trier) FROM meta_places
	INNER JOIN (
		SELECT meta_places.deedno_trier, COUNT(id_place_trier)
		FROM meta_places
		INNER JOIN (
			-- Urkunden, denen genau 1 Jahr zugeordnet ist
			SELECT deedno_trier
			FROM meta_cao
			WHERE
				EXTRACT(YEAR FROM meta_cao.date_to)
				- EXTRACT(YEAR FROM meta_cao.date_from)
				<= 0
		) AS uy ON meta_places.deedno_trier = uy.deedno_trier
		GROUP BY meta_places.deedno_trier
		HAVING COUNT(id_place_trier) = 1
	) AS dt ON meta_places.deedno_trier = dt.deedno_trier
	GROUP BY id_place_trier
) AS ud ON places.id_place_trier = ud.id_place_trier
WHERE x > 0 OR y > 0
ORDER BY count DESC, place ASC