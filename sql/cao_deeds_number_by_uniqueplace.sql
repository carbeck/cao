﻿-- Städte nach Anzahl der ortseindeutigen Urkunden
SELECT place, x, y, ud.count
FROM places
JOIN (
	-- Urkunden, denen genau 1 Ort zugewiesen ist
	SELECT id_place_trier, COUNT(id_place_trier) FROM meta_places
	JOIN (
		SELECT deedno_trier, COUNT(id_place_trier)
		FROM meta_places
		GROUP BY deedno_trier
		HAVING COUNT(id_place_trier) = 1
	) AS dt ON meta_places.deedno_trier = dt.deedno_trier
	GROUP BY id_place_trier
) AS ud ON places.id_place_trier = ud.id_place_trier
WHERE x > 0 OR y > 0
ORDER BY count DESC, place ASC