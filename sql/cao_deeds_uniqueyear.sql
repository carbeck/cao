﻿-- Urkunden, denen genau 1 Jahr zugeordnet ist
SELECT deedno_trier
FROM meta_cao
WHERE
	EXTRACT(YEAR FROM meta_cao.date_to)
	- EXTRACT(YEAR FROM meta_cao.date_from)
	<= 0