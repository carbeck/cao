#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# GENERAL STUFF ================================================================

import argparse
import csv
import json
import re
import sys

# FUNCTIONS ==================================================================== 

def stats(rows, search_in, patterns, ignore_case):
    """Search the provided record rows for provided patterns"""

    # Initialize list of dictionaries to hold the statistics for each place
    stats = []

    # Set of place names
    places = set()

    # Read the record list row by row
    for row in rows:

        # If the current place is not yet in stats, create it, along with its 
        # associated longitude and latitude, and a field initializing the total
        # count for occurrences of the place
        if row['place'] not in places:
            places.add(row['place'])
            stats.append({
                'place' : row['place'],
                'x'     : row['x'],
                'y'     : row['y'],
                'total' : 0,
            })
        
        # Test patterns for the current row
        for label, pattern in patterns:

            # Cast the pattern string as raw and compile it
            if ignore_case == False:
                p = re.compile(r'{}'.format(pattern))
            else:
                p = re.compile(r'{}'.format(pattern), re.I)

            # Get the index of the record containing data for the current place
            # in the list of place statistics
            i = dict([(f['place'], i) for (i, f) in enumerate(stats)])[row['place']]

            # Add current label to place record and set it to 0 if it doesn't 
            # exist yet (you can't increase unintialized variables)
            if label not in stats[i]:
                stats[i].update({label : 0})

            # If successful, increase the counter for the current pattern in 
            # the stats record for the current place by one, as well as the 
            # total for this place
            if bool(p.search(row[search_in])) is True:
                stats[i].update({
                    label   : stats[i][label] + 1,
                    'total' : stats[i]['total'] + 1,
                })

    # Return statistics ordered by place as a list of dictionaries
    return stats

# MAIN =========================================================================

def main(argv=None):
    """Main function providing command line option parser."""
    if argv is None:
        argv = sys.argv[1:]
    
    # Parser for command line options
    parser = argparse.ArgumentParser(
        description="""Stats for places from rows in CSV file. The existence of 
            columns named 'place', 'x' (±0.0 [°E]), and 'y' (±0.0 [°N]) is 
            presupposed."""
    )

    parser.add_argument(
        'infile',
        nargs='?',
        type=argparse.FileType('r'),
        # default=sys.stdin,
        help='''Input file''',
    )
    parser.add_argument(
        '-i', '--ignore-case',
        action='store_true',
        default=False,
        help='''Ignore capitalization'''
    )
    parser.add_argument(
        '-j', '--json',
        action='store_true',
        default=False,
        help='''Print result as JSON array rather than CSV'''
    )
    parser.add_argument(
        '-p', '--pattern',
        type=str,
        nargs=2,
        metavar=('LABEL', "'REGEX'"),
        dest='patterns',
        action='append',
        required=True,
        help="""A pair of label and regex to group records by. 
            This option must be called multiple times for using multiple 
            fields.""",
    )
    parser.add_argument(
        '-s', '--search-in',
        metavar='FIELDNAME',
        default='wordform',
        type=str,
        help="""The name of the field to be searched in (default: 
            %(default)s)""",
    )

    # Parse arguments
    args = parser.parse_args(argv)

    # Read input file
    keys = []
    lines = []
    with args.infile as f:

        # Analyze the file to find out dialect
        dialect = csv.Sniffer().sniff(f.read(1024))
        f.seek(0)

        # Read CSV as _csv.reader object
        reader = csv.reader(f, dialect)

        # Read header (assumes that the file *has* a header!)
        keys = next(reader)

        # Read lines as a list of dictionaries
        for line in reader:
            lines.append(dict(zip(keys, line)))

    # Aggregate the statistics
    result = stats(lines, args.search_in, args.patterns, args.ignore_case)

    # Output
    if args.json:
        # Output as JSON array
        return '{}\n'.format(json.dumps(result))
    else:
        # Output as CSV
        rows = []
        keys = ['place', 'x', 'y', 'total'] + [l for l, p in args.patterns]
        
        for row in result:
            rows.append([row[key] for key in keys])

        rows = sorted(rows)
        rows.insert(0, keys)

        return '\n'.join([
            ', '.join([
                '"{}"'.format(str(i)) for i in row
            ]) for row in rows
        ]) + '\n'

# Execute the program
if __name__ == '__main__':

    # Returning output to stdout
    sys.stdout.write(main())

    # Close program
    sys.exit()
