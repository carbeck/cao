#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# GENERAL STUFF ================================================================

import argparse
import credentials
import json
import psycopg2
import psycopg2.extras
import sys

# FUNCTIONS ====================================================================

def getmeta(number, regex=False):
    '''Return meta information on a deed as a dictionary object'''

    # Establish DB connection
    try:
        conn = psycopg2.connect(
            host=credentials.HOST, 
            dbname=credentials.DB,
            user=credentials.USER,
            password=credentials.PASS
        )
    
    except psycopg2.OperationalError as e:
        sys.exit('ERROR: {}'.format(e))
    
    # Establishing DB cursor
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    
    if regex == False:
        comp = '='
    else:
        comp = '~'

    query = '''
        SELECT meta_cao.date_from,
            meta_cao.date_to,
            meta_cao.date_note,
            places.place,
            places.x,
            places.y,
            CONCAT('http://tcdh01.uni-trier.de/cgi-bin/iCorpus/CorpusIndex.tcl?hea=qf&for=qfcoraltdu&cnt=qfcoraltdu&xid=', meta_cao.deedno_trier) AS link_trier
        FROM meta_cao, (
            SELECT id_place_trier
            FROM meta_cao
            INNER JOIN meta_places
                ON meta_cao.deedno_cao_long = meta_places.deedno_cao_long
            WHERE meta_cao.deedno_cao_long {} %(number)s
        ) AS placeids
        JOIN places ON placeids.id_place_trier = places.id_place_trier
        WHERE meta_cao.deedno_cao_long {} %(number)s
    '''.format(comp, comp)

    # Execute query
    cur.execute(query, {'number': number})
    
    # Fetch all the results
    result = []
    for row in cur.fetchall():
        result.append(dict(row))

    # Make dates strings instead of datetime objects (JSON can't deal with those)
    for i in result:
        i["date_from"] = str(i["date_from"])
        i["date_to"] = str(i["date_to"])
    
    # Close cursor
    cur.close()
    
    # Close DB connection
    conn.close()

    return result

# MAIN =========================================================================

def main(argv=None):
    """Main function providing command line option parser."""
    if argv is None:
        argv = sys.argv[1:]
    
    # Parser for command line options
    parser = argparse.ArgumentParser(
        description='''Return the text of a deed''')
    parser.add_argument('number', help='''Deed number according to CAO (e.g., 
        123, 123-A, 123-01,  N-123-45-a, N-123-A-45-b).''')
    parser.add_argument('-j', '--json', action='store_true', default=False, 
        help='''Print result as JSON array rather than text''')
    parser.add_argument('-r', '--regex', action='store_true', default=False,
        help='''Enter deed number as a regular expression, e.g. N-647 to return
        N-647-1982-b, or ^222 to return 222-A and 222-B. Entering just 222
        contains any deed numbers with '222' in them.''')
    args = parser.parse_args(argv)

    # Do the query
    result = getmeta(args.number, args.regex)
    
    # Format output
    if args.json == True:
        # Output as JSON array
        return '{}\n'.format(json.dumps(result))

    elif len(result) > 1:
        # Output text for deeds with multiple place/time
        output = []
        for i, r in enumerate(result):
            for k in sorted({key: val for key, val in r.items() if val}):
                output += ['{} - {}: {}'.format(i+1, k, r[k])]
        return '{}\n'.format('\n'.join(output))

    else:
        # Output text for deeds with single place/time
        output = []
        for r in result:
            for k in sorted({key: val for key, val in r.items() if val}):
                output += ['{}: {}'.format(k, r[k])]
        return '{}\n'.format('\n'.join(output))

# Execute the program
if __name__ == '__main__':

    # Returning output to stdout
    sys.stdout.write(main())

    # Close program
    sys.exit()
