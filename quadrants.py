#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# GENERAL STUFF ================================================================

import argparse
import credentials
import psycopg2
import sys

# MAIN =========================================================================

def main(argv=None):
    """Main function providing command line option parser."""
    if argv is None:
        argv = sys.argv[1:]
        
    # Parser for command line options
    parser = argparse.ArgumentParser(description='''Query CAO database by 
        quadrant''')
    parser.add_argument('-q', '--quadrant', required=True, 
        help='''Quadrant (NW, NE, SW, SE)''')
    args = parser.parse_args(argv)
    
    # Define quadrants. VERY, VERY ROUGHLY, the river Main runs about along 
    # 50 °N, and the river Lech along 10.85 °E.
    if args.quadrant == 'NW':
        param = ['x<10.85', 'y>50.00']
    elif args.quadrant == 'NE':
        param = ['x>10.85', 'y>50.00']
    elif args.quadrant == 'SW':
        param = ['x<10.85', 'y<50.00']
    elif args.quadrant == 'SE':
        param = ['x>10.85', 'y<50.00']

    # Establish DB connection
    try:
        conn = psycopg2.connect(
            host=credentials.HOST, 
            dbname=credentials.DB,
            user=credentials.USER,
            password=credentials.PASS
        )

    except psycopg2.OperationalError as e:
        sys.exit('ERROR: {}'.format(e))

    # Establishing DB cursor
    cur = conn.cursor()

    # The query itself
    query = """
        SELECT DISTINCT deedno_cao_long
        FROM meta_places
        INNER JOIN places ON meta_places.id_place_trier = places.id_place_trier
        WHERE {} AND {}
        ORDER BY deedno_cao_long ASC
    """

    # Execute query
    cur.execute(query.format(param[0], param[1]))

    # Fetch all the results
    result = cur.fetchall()

    # Close cursor
    cur.close()

    # Close DB connection
    conn.close()

    return '{}\n'.format('\n'.join([str(x) for (x,) in result]))

# Execute the program
if __name__ == '__main__':

    # Returning output to stdout
    sys.stdout.write(main())

    # Close program
    sys.exit()
