#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# GENERAL STUFF ================================================================

import argparse
import credentials
import json
import psycopg2
import psycopg2.extras
import re
import sys

from pygments import highlight, lexers, formatters
from pygments_pprint_sql import SqlFilter

# FUNCTIONS ====================================================================

def grepdeeds(
        pattern,              # search pattern
        annotation=False,     # include annotation information
        full_text=False,      # search in full text, not line-by-line
        ignore_case=False,    # ignore case
        meta=False,           # include meta information
        only_matching=False,  # return only matching substrings
        regex=False,          # Parse pattern as regex
        print_query=False,    # only print query, do not make DB request
        exact=False,          # only deeds attributed to single date/location
        year=0                # fuzzy year
    ):
    '''Search CAO corpus for string patterns'''

    # Instantiate vars dictionary
    vars = {}

    # Update pattern string to dictionary for **kwargs filling in str.format().
    # This makes it possible to refer multiple times to the same value, since
    # we're building the query dynamically and you'd otherwise have to pass the
    # value of pattern multiple times depending on how often it's called in the 
    # string which is supposed to be formatted. This way we can later simply do
    # '{pattern} ... {pattern}'.format(**vars) and have the correct string 
    # appear in both places.
    vars['pattern'] = pattern

    # Register value for case (in)sensitive
    if ignore_case == True:
    	# case insensitive
        vars['insensitive'] = '*'
    else:
    	# case sensitive
        vars['insensitive'] = ''

    # Instantiate keyword strings
    Select = ['SELECT']
    From   = ['FROM']
    Join   = []
    Where  = ['WHERE']
    Order  = ['ORDER BY']

    # Make sure we're searching single lines, not in full text
    if full_text == False:
        Select.append('lines.row, lines.text')
        From.append('lines')
        Order.append('lines.deedno_cao_long, lines.vol, lines.page, lines.line')
    else:
        Select.append('fulltext.text')
        From.append('fulltext')
        Order.append('fulltext.deedno_cao_long')

    # If we're including meta information
    if meta == True or only_matching == True:
        Select.append(""", lines.deedno_cao_long AS deed,
            regexp_matches(LOWER(text), '({pattern})') AS wordform,
            places.place, places.x, places.y,
            CAST(EXTRACT(YEAR FROM meta_cao.date_from) AS integer) AS year_from,
            CAST(EXTRACT(YEAR FROM meta_cao.date_to) AS integer) AS year_to""")

    # Joins, part 1: combine the tables containing meta information on the
    # deeds and information on the places
    Join.append('''INNER JOIN meta_places ON (
            lines.deedno_cao_long = meta_places.deedno_cao_long)
        INNER JOIN meta_cao ON (
            meta_places.deedno_trier = meta_cao.deedno_trier)
        INNER JOIN (
            SELECT deedno_trier, COUNT(id_place_trier)
            FROM meta_places
            GROUP BY deedno_trier''')

    # Joins, part 2: if we're only searching distinct deeds
    if exact == True:
        Join.append("HAVING COUNT(id_place_trier) = 1")

    # Joins, part 3: the rest
    Join.append('''
        ) AS deedno_trier_unique ON (
            meta_places.deedno_trier 
            = deedno_trier_unique.deedno_trier)
        INNER JOIN places ON (
            meta_places.id_place_trier = places.id_place_trier)''')

    # If we're including annotation information
    if annotation == True:
        Select.append(''', ta.annotation, tl.lemma''')
        Join.append("""LEFT JOIN (
                SELECT
                    tokens.row,
                    tokens.annotation,
                    tokens.confidence_annotation
                FROM tokens
                WHERE
                    tokens.token ~{insensitive} '{pattern}'
                    AND tokens.confidence_annotation >= 0.95
            ) AS ta ON (lines.row = ta.row)
        """)
        Join.append("""LEFT JOIN (
                SELECT
                    tokens.row,
                    tokens.lemma,
                    tokens.confidence_lemma
                FROM tokens
                WHERE
                    tokens.token ~{insensitive} '{pattern}'
                    AND tokens.confidence_lemma >= 0.95
            ) AS tl ON (lines.row = tl.row)
        """)

    # Check whether plain text or regex
    if regex == False:
        # Plain text search
        Where.append("text ~~{insensitive} '%{pattern}%'")
    else:
        # Regex search
        Where.append("text ~{insensitive} '{pattern}'")

    # Again, if we're only searching distinct deeds
    if exact == True:
        Where.append('''AND places.x != 0
            AND EXTRACT(YEAR FROM meta_cao.date_to) - EXTRACT(YEAR FROM 
            meta_cao.date_from) <= {}'''.format(year))

    # Glue together query statement
    query = (' '.join([re.sub(r'\s{2,}', ' ', ' '.join(x)) 
        for x in [Select, From, Join, Where, Order]]
    )).format(**vars)

    # If we only want to return the query statement but not execute it
    # (mainly for debugging purposes; pretty-printing included)
    if print_query == True:
        lexer = lexers.MySqlLexer()
        lexer.add_filter(SqlFilter())
        return highlight(query, lexer, formatters.TerminalFormatter())

    # Establish DB connection
    try:
        conn = psycopg2.connect(
            host=credentials.HOST, 
            dbname=credentials.DB,
            user=credentials.USER,
            password=credentials.PASS
        )
    
    except psycopg2.OperationalError as e:
        sys.exit('ERROR: {}'.format(e))
    
    # Establish DB cursor
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    # Execute query
    cur.execute(query)
    
    # Fetch all the results, remove duplicates on the fly
    result = []
    for row in cur.fetchall():
        if dict(row) not in result:
           result.append(dict(row))

    # Close cursor
    cur.close()
    
    # Close DB connection
    conn.close()

    return result

# MAIN =========================================================================

def main(argv=None):
    """Main function providing command line option parser."""
    if argv is None:
        argv = sys.argv[1:]
    
    # Parser for command line options
    parser = argparse.ArgumentParser(
        description='''Search for a word in the CAO corpus'''
    )
    
    parser.add_argument(
        '-a', '--annotation',
        action='store_true',
        default=False,
        help='''Return auto-generated morphological annotations at confidence
            level >= 0.95 for annotation and lemmatization'''
    )
    parser.add_argument(
        '-f', '--full-text',
        action='store_true',
        default=False,
        help='''Search in full text instead of lines'''
    )
    parser.add_argument(
        '-i', '--ignore-case',
        action='store_true',
        default=False,
        help='''Ignore capitalization'''
    )
    parser.add_argument(
        '-j', '--json',
        action='store_true',
        default=False,
        help='''Print result as JSON array rather than CSV'''
    )
    parser.add_argument(
        '-m', '--meta',
        action='store_true',
        default=False,
        help='''Also return meta information for a match'''
    )
    parser.add_argument(
       '-o', '--only-matching',
       action='store_true',
       default=False,
       help='''Return only the matching substring'''
    )
    parser.add_argument(
        '-q', '--print-query',
        action='store_true',
        default=False,
        help='''Print query statement for troubleshooting and exit (i.e. don't 
            retrieve data from the DB)'''
    )
    parser.add_argument(
        '-r', '--regex',
        action='store_true',
        default=False,
        help='''Search as regular expression instead of plain text 
            (https://www.postgresql.org/docs/current/functions-matching.html)'''
    )
    parser.add_argument(
        '-x', '--exact',
        action='store_true',
        default=False,
        help='''Only consider those deeds which are attributed to one year and 
            a single place'''
    )
    parser.add_argument(
        '-y', '--year',
        default=0,
        metavar='N',
        help='''Fuzz year by N years (to be used with -x/--exact)'''
    )
    parser.add_argument(
        'pattern',
        type=str,
        nargs=1,
        help='''The search term'''
    )

    args = parser.parse_args(argv)

    # Do the query
    result = grepdeeds(
        pattern=args.pattern[0],
        annotation=args.annotation,
        full_text=args.full_text,
        ignore_case=args.ignore_case,
        meta=args.meta,
        only_matching=args.only_matching,
        regex=args.regex,
        print_query=args.print_query,
        exact=args.exact,
        year=args.year
    )
    
    # Format output
    if args.json == True:
        # Output as JSON array
        return '{}\n'.format(json.dumps(result))

    elif type(result) is str:
    	# For when we're just returning the query statement
        return '{}\n'.format(result)

    elif args.only_matching == True:
        # Only return matching strings, nothing else. This is like grep -o.
        rows = []
        for row in result:
            rows.append(row['wordform'])

        return '\n'.join([
            ', '.join([
                '{}'.format(str(i)) for i in line
            ]) for line in rows
        ]) + '\n'

    else:
        # Since dict.values() is unordered and we want an ordered list for CSV
        # output, generate a list of lists for the result set.
        try:
            keys = sorted(result[0].keys())
            rows = []
            rows.append(keys)
        except IndexError as e:
            sys.exit('ERROR: {}'.format(e))
        
        for row in result:
            line = []
            for key in keys:
                # PostgreSQL returns regex matches as list, independent of how 
                # many results it contains. This is then returned as a string 
                # with angular brackets. This is kind of annoying, however.
                if type(row[key]) is not list:
                    line.append(row[key])
                else:
                    line.append(','.join(row[key]))
            rows.append(line)

        return '\n'.join([
            ', '.join([
                '"{}"'.format(str(i).strip()) for i in line
            ]) for line in rows
        ]) + '\n'

# Execute the program
if __name__ == '__main__':

    # Returning output to stdout
    sys.stdout.write(main())

    # Close program
    sys.exit()
