CAO-Zeug
========

Skripte zur Auswertung des "Corpus der altdeutschen Originalurkunden bis zum Jahr 1300". Das Vorhandensein der CAO-Datenbank wird vorausgesetzt. Die Zugangsdaten zur PostgreSQL-Datenbank sind nach der Installation der Datenbank in `credentials.py` analog zur Beispieldatei einzutragen. `python3 <Programmname> -h` gibt Informationen zur Benutzung.

| Dateiname                 | Zweck                                                                    |
| --------------------------|--------------------------------------------------------------------------|
| `deedtext.py`             | Text einer Urkunde aus der Datenbank ausgeben                            |
| `meta.py`                 | Metadaten zu einer Urkunde ausgeben                                      |
| `placestats.py`           | Merkmalsstatistik für Orte aus Belegliste generieren (mit Python-Regex)  |
| `quadrants.py`            | Zu jeweiligem Quadranten (NW/NE/SW/SE) gehörige Urkundennummern ausgeben |
| `searchword.py`           | String in der Datenbank suchen (als Reintext oder PostgreSQL-Regex)      |
| `places/fill_lat_long.py` | `cities_de_at_ch_lu_li_fr.csv` mit zugehörigen Ortskoordinaten befüllen  |

Da eine `__init__.py` vorhanden ist, können die einzelnen Skriptdateien auch gegenseitig mit ihren Funktionen als Module eingebunden werden, zum Beispiel in `foobar.py`:

``` python
from meta import getmeta
```

Die Funktion `getmeta()` kann nun auch in `foobar.py` verwendet werden.

## Inhalt des Ordners `sql`
Der Ordner enthält eine Sammlung von SQL-Skripten, die evtl. ganz nützlich zur Auswertung sind und teilweise über die Möglichkeiten der Python-Skripte hinausgehen.

## Inhalt des Ordners `places`
Die Dateien sind eine Sammlung von Ortskoordinaten für die Erstellung von Karten in QGIS o.ä. Die Orte können anhand der Populationsgröße als Referenzpunkte auf einer Karte angezeigt werden. Die CAO-Datenbank enthält ihre eigene Sammlung von Ortskoordinaten der Belegorte.

## DB in PostgreSQL importieren

``` shell
$ dropdb cao && createdb cao
$ gunzip < cao-YYYYMMDD.sql.gz | psql cao
```

## DB aus PostgreSQL exportieren

``` shell
$ pg_dump --no-owner cao | gzip > cao-YYYYMMDD.sql.gz
```

---------------------------------------

Carsten Becker (Humboldt-Universität zu Berlin)  
<carsten[dot]becker[at]hu-berlin[dot]de>  
2017–2024
