#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# GENERAL STUFF ================================================================

import argparse
import credentials
import psycopg2
import sys

# FUNCTIONS ====================================================================

def deedtext(number, full_text=False, regex=False):
    '''Read text of a deed from the database, return either line by line 
    (default) or as a running text'''

    # Establish DB connection
    try:
        conn = psycopg2.connect(
            host=credentials.HOST, 
            dbname=credentials.DB,
            user=credentials.USER,
            password=credentials.PASS
        )

    except psycopg2.OperationalError as e:
        sys.exit('ERROR: {}'.format(e))

    # Establishing DB cursor
    cur = conn.cursor()

    # Checking what to do
    if full_text == False:
        query = """SELECT row, text FROM lines WHERE deedno_cao_long"""

        # Checking if regex entry is selected
        if regex == False:
            query += """ = %s ORDER BY page, line ASC"""
        else:
            query += """ ~ %s ORDER BY deedno_cao_long, page, line ASC"""

    else:
        query = """SELECT text FROM fulltext WHERE deedno_cao_long"""

        # Checking if regex entry is selected
        if regex == False:
            query += """ = %s"""
        else:
            query += """ ~ %s"""

        query += """ ORDER BY deedno_cao_long ASC"""

    # Execute query
    cur.execute(query, (number,))

    # Fetch all the results
    result = cur.fetchall()

    # Close cursor
    cur.close()

    # Close DB connection
    conn.close()

    return result

# MAIN =========================================================================

def main(argv=None):
    """Main function providing command line option parser."""
    if argv is None:
        argv = sys.argv[1:]
        
    # Parser for command line options
    parser = argparse.ArgumentParser(
        description='''Return the text of a deed''')
    parser.add_argument('number', help='''Deed number according to CAO (e.g.,
        123, 123-A, 123-01, N-123-45-a, N-123-A-45-b).''')
    parser.add_argument('-r', '--regex', action='store_true', default=False,
        help='''Enter deed number as a regular expression, e.g. N-647 to return
        N-647-1982-b, or ^222 to return 222-A and 222-B. Entering just 222
        contains any deed numbers with '222' in them.''')
    parser.add_argument('-f', '--full-text', action='store_true', default=False,
        help='''Print as running text instead of line-by-line''')
    args = parser.parse_args(argv)

    result = deedtext(args.number, args.full_text, args.regex)

    if args.full_text == True:
        return '{}\n'.format(''.join([t for (t,) in result]))
    else:
        return '{}\n'.format('\n'.join(['{}\t{}'.format(r, l) for (r, l) in result]))

# Execute the program
if __name__ == '__main__':

    # Returning output to stdout
    sys.stdout.write(main())

    # Close program
    sys.exit()
